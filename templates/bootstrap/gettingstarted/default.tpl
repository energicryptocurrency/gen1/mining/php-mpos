 <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-info">
        <div class="panel-heading">
          <i class="fa fa-question fa-fw"></i> Getting Started Guide
        </div>
        <div class="panel-body">
        
    <p>1. <strong>Download Energi Miner.</strong></p>
      <ul>
        <li>
        Please visit the <a href="https://www.energi.world/downloads/" target="_blank">official download page</a>.
        </li>
        <li>
        The latest releases are also available <a href="https://github.com/energicryptocurrency/energiminer/releases" target="_blank">at GitHub</a>.
        </li>
      </ul>

    <p>2. <strong>Configure your miner.</strong></p>
       <ul>
         <li>
         Please follow the official <a href="https://www.energi.world/mining/" target="_blank">Energi Mining Guide</a>.
         </li>
         <li>
         Please pay special attention to hardware requirements and the FAQ sections.
         </li>
         <li>
         Please select appropriate stratum port:
            <ol>
            <li><strong>3105</strong> - diff 0.5 vardiff 0.1-256 (small rigs)</li>
            <li><strong>3110</strong> - diff 1.0 vardiff 0.1-256 (medium rigs)</li>
            <li><strong>3120</strong> - diff 2.0 vardiff 0.1-256 (large rigs)</li>
            <li><strong>3140</strong> - diff 4.0 vardiff 0.1-256 (extra-large rigs)</li>
            </ol>
         </li>
         <li>
         Please select appropriate location:
            <ol>
            <li><strong>us.mining.energi.network</strong> - US</li>
            <li><strong>asia.mining.energi.network</strong> - Asia</li>
            <li><strong>eu.mining.energi.network</strong> - Europe</li>
            </ol>
         </li>
       </ul>
       
    <p>3. <strong>Run your miner.</strong></p>
        <ul>
         <li>
         Example for US diff 0.5 for Nvidia (CUDA):
         <pre>
         energiminer "stratum://YourWalletAddress.RigName@us.mining.energi.network:3105" -U
         </pre>
         </li>
         <li>
         Example for Asia diff 2.0 for AMD (OpenCL):
         <pre>
         energiminer "stratum://YourWalletAddress.RigName@asia.mining.energi.network:3120" -G
         </pre>
         </li>
        </ul>

        </div>
      </div>
    </div>
    <!-- /.col-lg-12 -->
  </div>
