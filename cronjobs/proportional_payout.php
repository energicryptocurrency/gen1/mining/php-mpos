#!/usr/bin/php
<?php

/*

Copyright:: 2013, Sebastian Grewe

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

 */

// Change to working directory
chdir(dirname(__FILE__));

// Include all settings and classes
require_once('shared.inc.php');

// Check if we are set as the payout system
if ($config['payout_system'] != 'prop') {
  $log->logInfo("Please activate this cron in configuration via payout_system = prop");
  exit(0);
}

// Fetch all unaccounted blocks
$aAllBlocks = $block->getAllUnaccounted('ASC');
if (empty($aAllBlocks)) {
  $log->logDebug('No new unaccounted blocks found in database');
  $monitoring->endCronjob($cron_name, 'E0011', 0, true, false);
}

// Fetch precision
$precision = $coin->getCoinValuePrevision();
$table_precision = $coin->getCoinValuePrevision() + 3;

$count = 0;
// Table header for account shares
$strLogMask = "| %10.10s | %-5.5s | %15.15s | %15.15s | %12.12s | %12.12s | %${table_precision}.${table_precision}s | %${table_precision}.${table_precision}s | %${table_precision}.${table_precision}s | %${table_precision}.${table_precision}s |";
$log->logInfo(sprintf($strLogMask, 'Block', 'ID', 'Username', 'Valid', 'Invalid', 'Percentage', 'Payout', 'Donation', 'Fee', 'Bonus'));
foreach ($aAllBlocks as $iIndex => $aBlock) {
    $bHeight = $aBlock['height'];
    
  if ($aBlock['confirmations'] == -1) {
    $log->logInfo("Skipping orphan block {$aBlock['height']}");

    // Mark this block as accounted for
    if (!$block->setAccounted($aBlock['id'])) {
        $log->logFatal('Failed to mark block as accounted! Aborted! Error: ' . $block->getCronError());
        $monitoring->endCronjob($cron_name, 'E0014', 1, true);
    }

    continue;
  }

  if ($aBlock['confirmations'] < $config['confirmations']) {
    $log->logInfo("Not enough confirmations for {$aBlock['height']} ({$aBlock['confirmations']} < {$config['confirmations']}), postponing");
    break;
  }

  // Fetch last paid block information
  if ($iLastBlockId = $setting->getValue('last_accounted_block_id')) {
    $aLastAccountedBlock = $block->getBlockById($iLastBlockId);
  } else {
    // A fake block to ensure payouts get started on first round
    $iLastBlockId = 0;
    $aLastAccountedBlock = array('height' => 0, 'confirmations' => 1);
  }

  // Ensure we are not paying out twice, ignore if the previous paid block is orphaned (-1 confirmations) and payout anyway
  if ((!$aBlock['accounted'] && $bHeight > $aLastAccountedBlock['height']) || (@$aLastAccountedBlock['confirmations'] == -1)) {
    $aAccountShares = $share->getSharesForAccounts($bHeight);
    $iRoundShares = $share->getRoundShares(0, $bHeight);
    $config['reward_type'] == 'block' ? $dReward = $aBlock['amount'] : $dReward = $config['reward'];

    if (empty($aAccountShares)) {
        $log->logError('No shares found for this block, aborted: ' . $bHeight. ' '.$share->getCronError());
        //$monitoring->endCronjob($cron_name, 'E0013', 1, true);
      
        // Mark this block as accounted for
        if (!$block->setAccounted($aBlock['id'])) {
            $log->logFatal('Failed to mark block as accounted! Aborted! Error: ' . $block->getCronError());
            $monitoring->endCronjob($cron_name, 'E0014', 1, true);
        }
        continue;
    }
    
    //===========
    $write_conn = $mysqli->write_conn();
    $res = $write_conn->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);
    
    if (!$res) {
      $log->logFatal('Unable to start transaction for: ' . $bHeight. ' '.$write_conn->error);
      $monitoring->endCronjob($cron_name, 'E1001', 1, true);
    }
    //===========

    // This may change after initial block is found
    $block->setShares($aBlock['id'], $iRoundShares);

    // Loop through all accounts that have found shares for this round
    foreach ($aAccountShares as $key => $aData) {
      // Skip users with only invalids
      if ($aData['valid'] == 0) {
        continue;
      }
      // Skip entries that have no account ID, user deleted?
      if (empty($aData['id'])) {
        $log->logInfo('User ' . $aData['username'] . ' does not have an associated account, skipping');
        continue;
      }

      // Defaults
      $aData['fee' ] = 0;
      $aData['donation'] = 0;
      $aData['pool_bonus'] = 0;
      $aData['percentage'] = ( 100 / $iRoundShares ) * $aData['valid'];
      $aData['payout'] = ( $aData['percentage'] / 100 ) * $dReward;

      // Calculate pool fees if they apply
      if ($config['fees'] > 0 && $aData['no_fees'] == 0)
        $aData['fee'] = $config['fees'] / 100 * $aData['payout'];

      // Calculate pool bonus if it applies, will be paid from liquid assets!
      if ($config['pool_bonus'] > 0) {
        if ($config['pool_bonus_type'] == 'block') {
          $aData['pool_bonus'] = ( $config['pool_bonus'] / 100 ) * $dReward;
        } else {
          $aData['pool_bonus'] = ( $config['pool_bonus'] / 100 ) * $aData['payout'];
        }
      }

      // Calculate donation amount, fees not included
      $aData['donation'] = $user->getDonatePercent($user->getUserId($aData['username'])) / 100 * ( $aData['payout'] - $aData['fee']);

      // Verbose output of this users calculations
      $log->logInfo(
        sprintf($strLogMask, $bHeight, $aData['id'], $aData['username'], $aData['valid'], $aData['invalid'],
                number_format($aData['percentage'], $precision), number_format($aData['payout'], $precision), number_format($aData['donation'], $precision), number_format($aData['fee'], $precision), number_format($aData['pool_bonus'], $precision))
      );

      // Update user share statistics
      if (!$statistics->updateShareStatistics($aData, $aBlock['id']))
        $log->logFatal('Failed to update share statistics for ' . $aData['username'] . ': ' . $statistics->getCronError());
      // Add new credit transaction
      if (!$transaction->addTransaction($aData['id'], $aData['payout'], 'Credit', $aBlock['id']))
        $log->logFatal('Failed to insert new Credit transaction to database for ' . $aData['username'] . ': ' . $transaction->getCronError());
      // Add new fee debit for this block
      if ($aData['fee'] > 0 && $config['fees'] > 0)
        if (!$transaction->addTransaction($aData['id'], $aData['fee'], 'Fee', $aBlock['id']))
          $log->logFatal('Failed to insert new Fee transaction to database for ' . $aData['username'] . ': ' . $transaction->getCronError());
      // Add new donation debit
      if ($aData['donation'] > 0)
        if (!$transaction->addTransaction($aData['id'], $aData['donation'], 'Donation', $aBlock['id']))
          $log->logFatal('Failed to insert new Donation transaction to database for ' . $aData['username'] . ': ' . $transaction->getCronError());
      // Add new bonus credit
      if ($aData['pool_bonus'] > 0)
        if (!$transaction->addTransaction($aData['id'], $aData['pool_bonus'], 'Bonus', $aBlock['id']))
          $log->logFatal('Failed to insert new Bonus transaction to database for ' . $aData['username'] . ': ' . $transaction->getCronError());
    }

    // Add block as accounted for into settings table
    $setting->setValue('last_accounted_block_id', $aBlock['id']);
    
    //===========
    $res = $write_conn->commit();
    
    if (!$res) {
      $log->logFatal('Commit failure for: ' . $bHeight. ' '.$write_conn->error);
      $monitoring->endCronjob($cron_name, 'E1002', 1, true);
    }

    //===========

    // Move counted shares to archive before this blockhash upstream share
    if (!$share->moveArchive($aBlock['id'], $bHeight))
      $log->logError("Failed to copy shares to archive for height " . $bHeight . ": " . $share->getCronError());
    // Delete all accounted shares
    if (!$share->deleteAccountedShares($bHeight)) {
      $log->logFatal('Failed to delete accounted shares for height ' . $bHeight . ', aborted! Error: ' . $share->getCronError());
      $monitoring->endCronjob($cron_name, 'E0016', 1, true);
    }
    // Mark this block as accounted for
    if (!$block->setAccounted($aBlock['id'])) {
      $log->logFatal('Failed to mark block as accounted! Aborted! Error: ' . $block->getCronError());
      $monitoring->endCronjob($cron_name, 'E0014', 1, true);
    }
  } else {
    $log->logFatal('Potential double payout detected for block ' . $aBlock['id'] . '. Aborted.');
    $aMailData = array(
      'email' => $setting->getValue('system_error_email'),
      'subject' => 'Payout Failure: Double Payout',
      'Error' => 'Possible double payout detected',
      'BlockID' => $aBlock['id'],
      'Block Height' => $bHeight,
      'Block Share ID' => $aBlock['share_id']
    );
    if (!$mail->sendMail('notifications/error', $aMailData))
      $log->logError('Failed to send notification mail');
    $monitoring->endCronjob($cron_name, 'E0015', 1, true);
  }
}

require_once('cron_end.inc.php');
