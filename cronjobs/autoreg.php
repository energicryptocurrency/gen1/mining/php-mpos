#!/usr/bin/php
<?php


/*

Copyright:: 2018, Andrey Galkin, Energi Core

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

// Change to working directory
chdir(dirname(__FILE__));

// Include all settings and classes
require_once('shared.inc.php');

$log->logInfo("Processing sharing with no account/worker association");
$orphan_shares = $share->getWithNoOwner();

foreach ($orphan_shares as $s) {
    $share_name = $s['share_name'];
    list($account_name, $worker_name) = explode('.', $share_name, 2);
    $account_name = substr($account_name, 0, 34);
    $worker_name = substr($worker_name, 0, 10);

    if (($s['account_name'] === null) && !$user->getUserId($account_name)) {
        $log->logInfo("Auto-creating account: {$account_name}");
        
        $res = $user->autoRegister($account_name);
        
        if (!$res) {
            $log->logFatal('    unable to create user. ERROR: ' . $user->getCronError());
            $monitoring->endCronjob($cron_name, 'E0010', 1, true);
        }
    }

    if ($s['worker_name'] === null) {
        $log->logInfo("Auto-creating worker: {$share_name}");

        $user_id = $user->getUserId($account_name);
        $res = $worker->addWorker($user_id, $worker_name, 'default');
        
        if (!$res) {
            $log->logInfo('    unable to create worker. ERROR: ' . $worker->getCronError());
            //$monitoring->endCronjob($cron_name, 'E0010', 1, true);
        }
    }
}

require_once('cron_end.inc.php');
